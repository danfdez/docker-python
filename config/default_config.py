# default configuration module to set up flask and gunicorn config when the app is loaded

import os
import multiprocessing


SET_ENVIRONMENT_OPTIONS = {
    "DEVELOP": {
        "debug": True,
        "reload": True,
        "timeout": 300,
        "worker_class": "sync",
        "log_level": "debug",
    },
    "PRODUCTION": {
        "debug": False,
        "reload": False,
        "timeout": 10,
        "worker_class": "gevent",
        "log_level": "error",
    },
}
ENVIRONMENT_OPTIONS = SET_ENVIRONMENT_OPTIONS.get(os.getenv("ENVIRONMENT"))


class Config(object):
    """
    Flask and System configuration
    """

    DEBUG = ENVIRONMENT_OPTIONS.get("debug")
    MAX_WORKERS = multiprocessing.cpu_count()
    PORT = os.getenv("PORT")
    ENVIRONMENT = os.getenv("ENVIRONMENT")

    """
    Gunicorn configuration
    """
    GUNICORN = {
        "HOST": "0.0.0.0",
        "PORT": PORT,
        "MAX_REQUESTS": 1000,
        "WORKER_CLASS": ENVIRONMENT_OPTIONS.get("worker_class"),
        "WORKERS": MAX_WORKERS,
        "RELOAD": ENVIRONMENT_OPTIONS.get("reload"),
        "RELOAD_ENGINE": "inotify",
        "PRELOAD_APP": False,
        "TIMEOUT": ENVIRONMENT_OPTIONS.get("timeout"),
        "CAPTURE_OUTPUT": True,
        "ERROR_LOG_FILE": "logs/gunicorn.log",
        "LOG_LEVEL": ENVIRONMENT_OPTIONS.get("log_level"),
    }
