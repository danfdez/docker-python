# module to call from docker-compose to set gunicorn app configuration

from app import app


bind = app.config["GUNICORN"]["HOST"] + ":" + str(app.config["GUNICORN"]["PORT"])
max_requests = app.config["GUNICORN"]["MAX_REQUESTS"]
worker_class = app.config["GUNICORN"]["WORKER_CLASS"]
workers = app.config["GUNICORN"]["WORKERS"]
reload = app.config["GUNICORN"]["RELOAD"]
reload_engine = app.config["GUNICORN"]["RELOAD_ENGINE"]
preload_app = app.config["GUNICORN"]["PRELOAD_APP"]
timeout = app.config["GUNICORN"]["TIMEOUT"]
loglevel = app.config["GUNICORN"]["LOG_LEVEL"]
errorlog = app.config["GUNICORN"]["ERROR_LOG_FILE"]
