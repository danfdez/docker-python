from flask import Flask
from config.default_config import Config


app = Flask(__name__)
config = Config()
app.config.from_object(config)


@app.route("/")
def hello():
    return "HELLO FROM FLASK WITH GUNICORN"

# TODO info like this should appear automatically need to show logs to stdout
# show basic info in stdout
# print("RUNNING IN: {}:{}".format(app.config["GUNICORN"]["HOST"], app.config["GUNICORN"]["PORT"]))
# print("ENVIRONMENT: {}".format(app.config["ENVIRONMENT"]))
# print("LOG LEVEL: {}".format(app.config["GUNICORN"]["LOG_LEVEL"]))
# print("LOG FILE: {}".format(app.config["GUNICORN"]["ERROR_LOG_FILE"]))
